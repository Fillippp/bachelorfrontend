import { Component } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root', //Name mit dem ich auf die Komponente zugreifen kann
  templateUrl: './app.component.html', //Stelle den Inhalt aus der ...html Datei
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(private apiService: ApiService) {}
  helloWorld = this.apiService.getHelloWorld();
  title = 'Frontend';
}
