import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getHelloWorld() {
    return this.http
      .get('http://localhost:8080/', { responseType: 'text' })
      .pipe(map((response) => response));
  }
}
